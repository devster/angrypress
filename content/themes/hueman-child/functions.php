<?php
/* ------------------------------------------------------------------------- *
 *  Custom functions
/* ------------------------------------------------------------------------- */

// Add your custom functions here, or overwrite existing ones. Read more how to use:
// http://codex.wordpress.org/Child_Themes

function the_breadcrumb() {
	if ( ! is_home() ) {
		echo '<a href="';
		echo get_option( 'home' );
		echo '">';
		bloginfo( 'name' );
		echo '</a> Ã‚Â» ';
		if ( is_category() || is_single() ) {
			the_category( 'title_li=' );
			if ( is_single() ) {
				echo ' Ã‚Â» ';
				the_title();
			}
		} elseif ( is_page() ) {
			echo the_title();
		}
	}
}

/*  IE js header
/* ------------------------------------ */
if ( ! function_exists( 'alx_ie_js_header' ) ) {

	function alx_ie_js_header () {
		echo '<!--[if lt IE 9]>'. "\n";
		echo '<script src="cdnjs.cloudflare.co/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>'."\n";
//    echo '<script src="' . esc_url( get_template_directory_uri() . '/js/ie/html5.js' ) . '"></script>'. "\n";
		echo '<script src="' . esc_url( get_template_directory_uri() . '/js/ie/selectivizr.js' ) . '"></script>'. "\n";
		echo '<![endif]-->'. "\n";
	}
	
}
add_action( 'wp_head', 'alx_ie_js_header' );

// add ie conditional html5 shim to header
// function add_ie_html5_shim () {
//     echo '<!--[if lt IE 9]>';
//     echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
//     echo '<![endif]-->';
// }
// add_action('wp_head', 'add_ie_html5_shim');

/*  IE js footer
/* ------------------------------------ */
if ( ! function_exists( 'alx_ie_js_footer' ) ) {

	function alx_ie_js_footer () {
		echo '<!--[if lt IE 9]>'. "\n";
		echo '<script src="' . esc_url( get_template_directory_uri() . '/js/ie/respond.js' ) . '"></script>'. "\n";
		echo '<![endif]-->'. "\n";
	}
}
add_action( 'wp_footer', 'alx_ie_js_footer', 20 );

/*  Enqueue javascript
/* ------------------------------------ */
if ( ! function_exists( 'jquery_scripts' ) ) {

	function alx_scripts() {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', 'http' . ($_SERVER['SERVER_PORT'] == 443 ? 's' : '') . '://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js', false, '1.11.2' );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/js/jquery.flexslider.min.js', array( 'jquery' ),'', false );
		wp_enqueue_script( 'jplayer', get_template_directory_uri() . '/js/jquery.jplayer.min.js', array( 'jquery' ),'', true );
		wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', array( 'jquery' ),'', true );
		if ( is_singular() ) { wp_enqueue_script( 'sharrre', get_template_directory_uri() . '/js/jquery.sharrre.min.js', array( 'jquery' ),'', true ); }
		if ( is_singular() && get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
	}
}
add_action( 'wp_enqueue_scripts', 'alx_scripts' );

function hueman_header_image() {
	do_action( 'hueman_header_image' );
}

if ( ! function_exists( 'alx_site_title' ) ) {

	function alx_site_title() {

		// Text or image?
		if ( ot_get_option( 'custom-logo' ) ) {
			$logo = '<img src="' . ot_get_option( 'custom-logo' ) . '" alt="' . get_bloginfo( 'name' ) . '">';
		} else {
			$logo = get_bloginfo( 'name' );
		}

		$link = '<a href="' . home_url( '/' ) . '" rel="home">' . $logo . '</a>';

		if ( is_front_page() || is_home() ) {
			$sitename = '<h1 class="site-image">' . $link . '</h1>' . "\n";
		} else {
			$sitename = '<p class="site-image">' . $link . '</p>' . "\n";
		}

		return $sitename;
	}

}

/*  Social links
/* ------------------------------------ */
if ( ! function_exists( 'alx_social_links' ) ) {

	function alx_social_links() {
		if ( !ot_get_option('social-links' ) == '' ) {
			$links = ot_get_option( 'social-links', array() );
			if ( !empty( $links ) ) {
				echo '<ul class="social-links">';
				foreach ( $links as $item ) {

					// Build each separate html-section only if set
					if ( isset($item['title']) && !empty($item['title']) )
						{ $title = 'title="' .$item['title']. '"'; } else $title = '';
					if ( isset($item['social-link']) && !empty($item['social-link']) )
						{ $link = 'href="' .$item['social-link']. '"'; } else $link = '';
					if ( isset($item['social-target']) && !empty($item['social-target']) )
						{ $target = 'target="' .$item['social-target'][0]. '"'; } else $target = '';
					if ( isset($item['social-icon']) && !empty($item['social-icon']) )
						{ $icon = 'class="fa ' .$item['social-icon']. '"'; } else $icon = '';
					if ( isset($item['social-color']) && !empty($item['social-color']) )
						{ $color = 'style="color: ' .$item['social-color']. ';"'; } else $color = '';

					// Put them together
					if ( isset($item['title']) && !empty($item['title']) && isset($item['social-icon']) && !empty($item['social-icon']) && ($item['social-icon'] !='fa-') ) {
						echo '<li><a rel="nofollow" class="social-tooltip" '.$title.' '.$link.' '.$target.'><i '.$icon.' '.$color.'></i></a></li>';
					}
				}
				echo '</ul>';
			}
		}
	}	
}

if ( ! function_exists( 'alx_load' ) ) {

	function alx_load() {
		// Load theme languages
		load_theme_textdomain( 'hueman', get_template_directory().'/languages' );

		// Load theme options and meta boxes
		load_template( get_template_directory() . '/functions/theme-options.php' );//font options from menu
		load_template( get_template_directory() . '/functions/meta-boxes.php' );

		// Load custom widgets
		load_template( get_template_directory() . '/functions/widgets/alx-tabs.php' );
		load_template( get_template_directory() . '/functions/widgets/alx-video.php' );
		load_template( get_template_directory() . '/functions/widgets/alx-posts.php' );

		// Load custom shortcodes
		load_template( get_template_directory() . '/functions/shortcodes.php' );

		// Load dynamic styles
		load_template( get_template_directory() . '/functions/dynamic-styles.php' );//font options directory and links

		// Load TGM plugin activation
		load_template( get_template_directory() . '/functions/class-tgm-plugin-activation.php' );
	}

}
add_action( 'after_setup_theme', 'alx_load' );

function authors() {
	$display_admins = false;
	$order_by       = 'post_count'; // 'nicename', 'email', 'url', 'registered', 'display_name', or 'post_count'
	$order          = 'DESC';
	$role           = ''; // 'subscriber', 'contributor', 'editor', 'author' - leave blank for 'all'
	$avatar_size    = 161;
	$hide_empty     = false; // hides authors with zero posts

	if ( ! empty( $display_admins ) ) {
		$blogusers = get_users( 'orderby=' . $order_by . '&role=' . $role );
	} else {

		$admins  = get_users( 'role=administrator' );
		$exclude = array();

		foreach ( $admins as $ad ) {
			$exclude[] = $ad->ID;
		}

		$exclude   = implode( ',', $exclude );
		$blogusers = get_users( 'exclude=' . $exclude . '&orderby=' . $order_by . '&order=' . $order . '&role=' . $role );
	}

	$authors = array();
	foreach ( $blogusers as $bloguser ) {
		$user = get_userdata( $bloguser->ID );

		if ( ! empty( $hide_empty ) ) {
			$numposts = count_user_posts( $user->ID );
			if ( $numposts < 1 ) {
				continue;
			}
		}
		$authors[] = (array) $user;
	}

	echo '<ul id="grid-contributors">';
	foreach ( $authors as $author ) {
		$display_name       = $author['data']->display_name;
		$avatar             = get_avatar( $author['ID'], $avatar_size );
		$author_profile_url = get_author_posts_url( $author['ID'] );
		$description = get_user_meta( $author['description'] );

		echo '<div class="author-bio">';
		echo '<li class="single-item">';
		echo '<div class="bio-avatar"><a href="', $author_profile_url, '">', $avatar, '</a></div>';
		echo '<div class="author-name"><a href="', $author_profile_url, '" class="contributor-link">', $display_name, '</a></div>';
		echo '<div class="bio-desc">', $description;
		echo '</li>';
	}
	echo '</ul>';
}