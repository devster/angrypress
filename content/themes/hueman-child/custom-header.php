<?php
/**
 * Sample implementation of the Custom Header feature
 * http://codex.wordpress.org/Custom_Headers
 *
 * @package Hueman
 */

/**
 * Setup the WordPress core custom header feature.
 *
 * @uses hueman_admin_header_style()
 * @uses hueman_admin_header_image()
 */
function hueman_custom_header_setup() {
	$args = array(
		'flex-width'             => true,
		'width'                  => 300,
		'flex-height'            => true,
		'height'                 => 60,
		'default-text-color'     => 'ffffff',
		'wp-head-callback'       => 'hueman_header_style',
		'admin-head-callback'    => 'hueman_admin_header_style',
		'admin-preview-callback' => 'hueman_admin_header_image',
	);
	add_theme_support( 'custom-header', $args );
}
add_action( 'after_setup_theme', 'hueman_custom_header_setup' );

/**
 * Styles the header image displayed on the Appearance > Header admin panel.
 */
function hueman_admin_header_style(){ ?>

	<style type="text/css" media="screen">
		#header-preview {
			background-color: #33363b;
			overflow: hidden;
			padding: 0 25px;
		}
		#header-preview .site-title {
			font-size: 42px;
			font-weight: 600;
			letter-spacing: -0.5px;
			float: left;
			line-height: 60px;
			padding: 10px 0;
		}
		#header-preview .site-title a {
			display: block;
			max-width: 100%;
			text-decoration: none;
		}
		#header-preview .site-description {
			font-size: 16px;
			font-style: italic;
			font-weight: 100;
			float: left;
			margin-left: 20px;
			line-height: 60px;
			padding: 10px 0;
		}
	</style>
	<?php
	hueman_header_style();

}

function hueman_header_style() {
	$header_text_color = get_header_textcolor();
	$image = get_header_image();

	// If no custom options for text are set, let's bail
	// get_header_textcolor() options: HEADER_TEXTCOLOR is default, hide text (returns 'blank') or any hex value
	if ( HEADER_TEXTCOLOR == $header_text_color && ! $image ) {
		return;
	}

	// If we get this far, we have custom styles. Let's do this.
	?>
	<style type="text/css">

		<?php if ( 'blank' == $header_text_color ) : ?>
		.site-title.text-only,
		.site-description {
			position: absolute;
			clip: rect(1px, 1px, 1px, 1px);
		}
		.site-title.with-image a {
			position: relative;
			text-indent: -9999em;
		}
		<?php else : ?>
		.site-title a,
		.site-description {
			color: #<?php echo $header_text_color; ?>;
		}
		<?php endif; ?>

		<?php if ( $image ) : ?>
		.site-title a {
			height: <?php echo get_custom_header()->height; ?>px;
			width: <?php echo get_custom_header()->width; ?>px;
			background-image: url('<?php echo esc_url( $image ); ?>');
		}
		<?php endif; ?>
	</style>
<?php
}

/**
 * Custom header image markup displayed on the Appearance > Header admin panel.
 */
function hueman_admin_header_image(){

	echo '<div id="header-preview">';
	hueman_site_title();
	echo '</div>';

}

/**
 * Display the site header, using site title and site description
 * Replace title with the custom header image, if one exists.
 */
function hueman_site_title() {
	$class = get_header_image()? 'with-image': 'text-only';

	$link = '<a href="' . esc_url( home_url('/') ) . '" rel="home">' . get_bloginfo( 'name', 'display' ) . '</a>';

	if ( is_front_page() || is_home() ) {
		$sitename = '<h1 class="site-title ' . $class . '">' . $link . '</h1>'."\n";
	} else {
		$sitename = '<p class="site-title ' . $class . '">' . $link . '</p>'."\n";
	}

	echo '<div class="group pad">' . $sitename;

	if ( get_bloginfo( 'description' ) ) {
		echo '<p class="site-description">' . get_bloginfo( 'description' ) . '</p>';
	}

	echo '</div>';
}